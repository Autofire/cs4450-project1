/******************************************************************************
 *       file: Project1.java
 *     author: Daniel Edwards
 *      class: CS 4450
 * assignment: Program 1
 * 
 * last modified: 09/06/2020
 * 
 *   purpose:
 * This program displays shapes to the screen based on the given file.
 *
 * While the program is running, it can be controlled as such:
 *           E - Change color of [E]llipses
 *           C - Change color or [C]ircles
 *           L - Change color of [L]ines
 *           T - Toggle the [T]est line
 *         ESC - Close the program
 * 
 * While the test line is enabled, the following controls are also available:
 *  Arrow Keys - Move one of the line's points
 *           S - Take a [S]ample (into standard output)
 * 
 * Lines, circles, and ellipses can be drawn based on what is given in the file.
 * The file can have any number of lines, each one describing a shape.
 * Each line should either be empty or should be formatted as such:
 * 
 *  c x,y r
 * Generates a circle with center (x, y) and radius r.
 * 
 *  e x,y rx,ry
 * Generates an ellipse with center (x, y), x radius rx, and y radius ry.
 * 
 *  l x1,y1 x2,y2
 * Generates a line with endpoints (x1, y1) and (x2, y2).
 * 
 * Note that all numbers must be integers. The file's path is specified in
 * the constant named SHAPE_FILE_PATH.
 * 
 * Extra information may be printed for debugging purposes. However,
 * this information is printed only when the program starts, and only
 * if the DEBUG_INPUT or DEBUG_DRAW constants are set to true.
 * 
 ******************************************************************************/

package Project1;

import Project1.Shapes.Circle;
import Project1.Shapes.Ellipse;
import Project1.Shapes.Line;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import Project1.Shapes.Shape;
import Project1.Shapes.ShapeBuilder;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.Point;

/**
 * Container for the project's main code.
 */ 
// See purpose in code block above.
public class Project1 {

	// <editor-fold desc="Config">
	/** File which we use when looking for shapes. */
	private final static String SHAPE_FILE_PATH = "coordinates.txt";
	/** If true, print debug info while reading from the file. */
	private final static boolean DEBUG_INPUT = false;
	/** If true, print some debug info related to drawing. */
	private final static boolean DEBUG_DRAW = false;
	// </editor-fold>

	/** Our wrapper for input methods. */
	private InputManager input;

	/** This contains the shapes which we want to render. */
	private Shape[] shapes;


	// <editor-fold desc="Testing">
	/**
	 * Line used for testing purposes. This can be controlled with the
	 * arrow keys.
	 */
	private Line testLine;
	/** If true, the test line will be drawn. */
	private boolean drawTestLine;
	// </editor-fold>
	
	/**
	 * Non-static start point for the program itself.
	 */
	@SuppressWarnings("CallToPrintStackTrace")
	public void start()	{
		try {
			input = new InputManager(new int[]{
				Keyboard.KEY_ESCAPE,

				Keyboard.KEY_L, Keyboard.KEY_E, Keyboard.KEY_C,

				Keyboard.KEY_UP,   Keyboard.KEY_DOWN,
				Keyboard.KEY_LEFT, Keyboard.KEY_RIGHT,
				Keyboard.KEY_T,
				Keyboard.KEY_S
			});
			
			readShapeFile();
			setupTestLine();
			createWindow();
			initGL();
			//render();

			updateLoop();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Fills the shapes variable based on the contents of the file.
	 * @throws FileNotFoundException 
	 */
	private void readShapeFile() throws FileNotFoundException {

		if(DEBUG_INPUT) {
			System.out.println("Reading " + SHAPE_FILE_PATH);
		}

		ShapeBuilder builder = new ShapeBuilder();
		builder.setDebugFlags(DEBUG_INPUT, DEBUG_DRAW);
		
		List<Shape> shapeList = new ArrayList<>();

		File shapeFileObj = new File(SHAPE_FILE_PATH);
		try(Scanner shapeFileReader = new Scanner(shapeFileObj)) {
			while (shapeFileReader.hasNextLine()) {
				Shape newShape = builder.build(shapeFileReader.nextLine());
				if(newShape != null) {
					shapeList.add(newShape);
				}
			}
		}
		
		this.shapes = shapeList.toArray(new Shape[shapeList.size()]);

		if(DEBUG_INPUT) {
			System.out.println("Done reading");
			System.out.println();
		}

	}

	private void setupTestLine() {
		drawTestLine = false;
		testLine = new Line(new Point(400, 300), new Point(500, 400), Shape.Color.WHITE);
		testLine.setDrawEndpoints(true);
	}
	
	/**
	 * Creates the window and prepares for OpenGL.
	 * @throws Exception 
	 */
	private void createWindow() throws Exception{
		Display.setFullscreen(false);

		Display.setDisplayMode(new DisplayMode(640, 480));
		Display.setTitle("OpenGL Sandbox");
		Display.create();
	}

	/**
	 * Performs the base configuration of the OpenGL library.
	 */
	private void initGL() {
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		glOrtho(0, 640, 0, 480, 1, -1); // Wait isn't this backwards?

		glMatrixMode(GL_MODELVIEW);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	}

	/**
	 * Performs the update loop until the program terminates for some reason.
	 * This handles both input and rendering.
	 */
	private void updateLoop() {

		if(DEBUG_DRAW) {
			System.out.println("Beginning update loop...");
		}
		
		while(!Display.isCloseRequested() && !input.isDown(Keyboard.KEY_ESCAPE)) {
			getInput();
			render();
		}

		Display.destroy();
	}

	/**
	 * Processes input from the user. This should get called before any other
	 * step of the update loop.
	 */
	private void getInput() {

		input.update();

		if(input.isDown(Keyboard.KEY_T)) {
			drawTestLine = !drawTestLine;
			if(drawTestLine) {
				System.out.println("Enabling test line");
			}
			else {
				System.out.println("Disabling test line");
			}
		}
		if(drawTestLine) {
			int xMove = 0;
			int yMove = 0;
			if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) xMove++;
			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT))  xMove--;
			if(Keyboard.isKeyDown(Keyboard.KEY_UP))    yMove++;
			if(Keyboard.isKeyDown(Keyboard.KEY_DOWN))  yMove--;

			testLine.setP2(new Point(
				testLine.getP2().getX() + xMove,
				testLine.getP2().getY() + yMove
			));

			if(input.isDown(Keyboard.KEY_S)){
				testLine.debugNextDraw();
			}
		}

		if(input.isDown(Keyboard.KEY_L)) {
			for(Shape s : shapes) {
				if(s instanceof Line) {
					s.rotateColor();
				}
			}		
		}

		if(input.isDown(Keyboard.KEY_C)) {
			for(Shape s : shapes) {
				if(s instanceof Circle) {
					s.rotateColor();
				}
			}		
		}

		if(input.isDown(Keyboard.KEY_E)) {
			for(Shape s : shapes) {
				if(s instanceof Ellipse) {
					s.rotateColor();
				}
			}		
		}
	}
	
	/**
	 * Renders a single frame.
	 */
	private void render() {

			try{
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glLoadIdentity();
				glPointSize(1);
				
				glBegin(GL_POINTS);
					//glVertex2f(350, 150);
					//glVertex2f(50, 50);
					for(Shape s : shapes) {
						s.draw();
					}

					if(drawTestLine){
						testLine.draw();
					}
				glEnd();

				Display.update();
				Display.sync(60);
			}
			catch(Exception e){ }
	}
	
	/**
	 * Handles args and starts the graphical interface.
	 * @param args 
	 */
	public static void main(String[] args){
		Project1 basic = new Project1();
		basic.start();
	}
    
}
