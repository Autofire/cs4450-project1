/******************************************************************************
 *       file: InputManager.java
 *     author: Daniel Edwards
 *      class: CS 4450
 * assignment: Program1
 * 
 * last modified: 09/06/2020
 * 
 *   purpose:
 * See class's Javadoc, listed just below. With the exception of the main class,
 * standard Java documentation is used so that Netbeans can use it when giving
 * me a summary of the class. 
 * 
 ******************************************************************************/
package Project1;

import java.util.HashMap;
import org.lwjgl.input.Keyboard;

/**
 * Processes input, allowing for checking when something just gets pressed,
 * held, and depressed.
 * 
 * For this to work correctly, the update method must be called every frame.
 * 
 * Not really needed for this assignment, but I know I'll need it later.
 */
public class InputManager {

	/**
	 * This is a container for each of our keys.
	 * 
	 * Methods mirror the InputManager's methods, and should be called in the
	 * same circumstances.
	 */
	private class KeyInfo {
		private int keyCode;
		private boolean heldLastFrame;
		private boolean heldThisFrame;

		public KeyInfo(int keyCode) {
			this.keyCode = keyCode;
			heldLastFrame = false;
			heldThisFrame = false;
		}

		public void update() {
			heldLastFrame = heldThisFrame;
			heldThisFrame = Keyboard.isKeyDown(keyCode);
		}	

		public boolean isDown() {
			return heldThisFrame && !heldLastFrame;
		}

		public boolean isHeld() {
			return heldThisFrame;
		}

		public boolean isUp() {
			return !heldThisFrame && heldLastFrame;
		}
	}

	// If performance is ever a concern, this should be turned into a set of
	// parallel arrays. Will it make a big difference? Maybe not.
	// But we definitely lose on performance by doing a hash map for input.
	private HashMap<Integer, KeyInfo> watchList;

	/**
	 * Constructs a new input manager.
	 * @param keys List of keyboard keys to watch. (Use the Keyboard class for values.)
	 */
	public InputManager(int[] keys) {
		watchList = new HashMap<>();

		for(int key : keys) {
			watchList.put(key, new KeyInfo(key));
		}
	}

	/**
	 * Updates the info on our keys.
	 * Call this every frame to keep it up to date.
	 */
	public void update() {
		watchList.values().forEach(ki -> {
			ki.update();
		});
	}

	/**
	 * Checks if a key has just been pressed this frame.
	 * @param keyCode Key value from the Keyboard class.
	 * @return True if the key has just been pressed; false otherwise.
	 */
	public boolean isDown(int keyCode) {
		return watchList.get(keyCode).isDown();
	}

	/**
	 * Checks if a key is currently being pressed.
	 * @param keyCode Key value from the Keyboard class.
	 * @return True if the key is being pressed; false otherwise.
	 */
	public boolean isHeld(int keyCode) {
		return watchList.get(keyCode).isHeld();
	}

	/**
	 * Checks if a key is currently being pressed.
	 * @param keyCode Key value from the Keyboard class.
	 * @return True if the key is being pressed; false otherwise.
	 */
	public boolean isUp(int keyCode) {
		return watchList.get(keyCode).isUp();
	}
}
