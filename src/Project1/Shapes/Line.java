/******************************************************************************
 *       file: Line.java
 *     author: Daniel Edwards
 *      class: CS 4450
 * assignment: Program 1
 * 
 * last modified: 09/07/2020
 * 
 * See class's Javadoc, listed just below. With the exception of the main class,
 * standard Java documentation is used so that Netbeans can use it when giving
 * me a summary of the class. 
 * 
 ******************************************************************************/

package Project1.Shapes;

import org.lwjgl.util.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * This represents a line which may be drawn to the screen.
 * 
 * The line is stored as a pair of points.
 */
public class Line extends Shape {

	private Point p1;
	private Point p2;

	private boolean drawEndpoints;

	/**
	 * Constructs a new line object.
	 * 
	 * Note that the given points may be in any order, and they will be
	 * handled appropriately.
	 * 
	 * @param p1 First point.
	 * @param p2 Second point.
	 * @param drawColor Initial color used for drawing the line.
	 */
	public Line(Point p1, Point p2, Color drawColor) {
		super(drawColor);

		this.p1 = p1;
		this.p2 = p2;

		drawEndpoints = false;
	}
	
	@Override
	protected void specificDraw(boolean debugDraw) {
		//glPointSize(1);
		//glVertex2i(p1.getX(), p1.getY());
		//glVertex2i(p2.getX(), p2.getY());

		// We will draw from p1 to p2. According to the algorithm, we need
		//  dx: distance between x1 and x2
		//  dy: distance between y1 and y2
		//
		//  brushPos: position of pixel we're drawing
		//            (was x, y)
		//  brushOrthStep: change to brushPos on orthogonal steps
		//  brustDiagStep: change to brushPos on diagonal steps
		//
		//   d - "distance from the midpoint" but not quite
		//  incOrth: amount we'll change d on an orthogonal move
		//           (was incrementRight)
		//  incDiag: amount we'll change d on a diagonal move
		//           (was incrementUpRight)

		int dx = p2.getX() - p1.getX();
		int dy = p2.getY() - p1.getY();

		// The algorithm given in the slides ONLY WORKS when the slope is
		// between -1 and 1, i.e. the slope is "small." This is due to the
		// line having many pixels drawn per row... so orthogonal steps must
		// move along the X axis, not the Y axis.
		//
		// However, if the slope falls outside of -1 and 1, then it is climbing
		// on the Y axis faster than the X axis. Thus orthogonal steps must
		// be along the Y axis. This is where we largely deviate from the
		// given algorithm.
		//
		// That being said, we're also deviating anyway because our process
		// doesn't step just up and right... we can step left or down as well.
		float slope = dy / (float)dx;
		boolean smallSlope = slope <= 1 && slope >= -1;
		
		Point brushPos = new Point(p1);
		Point brushDiagStep = new Point(
			(dx > 0 ? 1 : -1),
			(dy > 0 ? 1 : -1)
		);
		Point brushOrthStep = new Point(
			( smallSlope ? brushDiagStep.getX() : 0),
			(!smallSlope ? brushDiagStep.getY() : 0)
		);

		int d, incOrth, incDiag;
		if(smallSlope) {
			// This is the case given in the slides
			d = 2*dy - dx;
			incOrth = Math.abs(2*dy);
			incDiag = -Math.abs(2*(dy-dx));

		}	
		else {
			// This is the other case, not the one given in the slides
			// The main thing is that we need to swap the cases for dx and dy.
			d = 2*dx - dy;
			incOrth = Math.abs(2*dx);
			incDiag = -Math.abs(2*(dx-dy));
		}

		if(slope < 0) {
			incDiag = -Math.abs(2*(dx+dy));
		}

		if(debugDraw) {
			System.out.println("Drawing line:");
			System.out.println("p1: " + p1);
			System.out.println("p2: " + p2);
			System.out.println();
			System.out.println("     dx,dy: " + dx + ", " + dy);
			System.out.println("     slope: " + slope);
			System.out.println("smallSlope: " + smallSlope);
			System.out.println();
			System.out.println("     brushPos: " + brushPos);
			System.out.println("brushOrthStep: " + brushOrthStep);
			System.out.println("brushDiagStep: " + brushDiagStep);
			System.out.println();
			System.out.println("      d: " + d);
			System.out.println("incOrth: " + incOrth);
			System.out.println("incDiag: " + incDiag);
			System.out.println();
		}

		
		if(debugDraw) {
			System.out.print("  Drawing first point: ");
			System.out.print("("+brushPos.getX()+", " + brushPos.getY()+") ");
			System.out.println("d: " + d);
		}
		boolean passedEnd = false;
		while(!passedEnd) {
			// Go ahead and draw the point
			glVertex2i(brushPos.getX(), brushPos.getY());

			
			if(d > 0) {
				// We need to move diagonally
				d += incDiag;
				brushPos.translate(brushDiagStep);
			}
			else {
				// We need to move orthogonally
				d += incOrth;
				brushPos.translate(brushOrthStep);
			}

			/*
			if(debugDraw) {
				System.out.print("Considering new point: ");
				System.out.print("("+brushPos.getX()+", " + brushPos.getY()+") ");
				System.out.println("d: " + d);
			}
*/

			// Now check if our most recent step pushed us passed the end.
			// Note that we only want to set this once we've gone PAST
			// the end, not reached it; if we did that then we'd stop early.
			if(smallSlope) {
				// Check x values
				if(dx > 0) {
					// We're moving in the positive direction; stay under.
					passedEnd = brushPos.getX() > p2.getX();
				}
				else {
					// We're moving in the negative direction; stay over.
					passedEnd = brushPos.getX() < p2.getX();
				}
			}
			else {
				// Check y values
				if(dy > 0) {
					passedEnd = brushPos.getY() > p2.getY();
				}
				else {
					passedEnd = brushPos.getY() < p2.getY();
				}
			}

		}

		if(debugDraw) {
			
			System.out.println("Finished drawing (last point skipped)");	
			System.out.println();
			System.out.println();
		}

		if(drawEndpoints){
			glEnd();
			glPointSize(5);
			glBegin(GL_POINTS);

			glVertex2i(p1.getX(), p1.getY());
			glVertex2i(p2.getX(), p2.getY());

			glEnd();
			glPointSize(1);
			glBegin(GL_POINTS);
		}
			
		
	}

	@Override
	public String toString() {
		return "Line{" + "p1=" + p1 + ", p2=" + p2 + '}';
	}
	

	public ReadablePoint getP1() {
		return p1;
	}

	public void setP1(ReadablePoint p1) {
		this.p1 = new Point(p1);
	}

	public ReadablePoint getP2() {
		return p2;
	}

	public void setP2(ReadablePoint p2) {
		this.p2 = new Point(p2);
	}

	public void setDrawEndpoints(boolean drawEndpoints) {
		this.drawEndpoints = drawEndpoints;	
	}
}
