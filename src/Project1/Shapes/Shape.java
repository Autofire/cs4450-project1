/******************************************************************************
 *       file: Shape.java
 *     author: Daniel Edwards
 *      class: CS 4450
 * assignment: Program 1
 * 
 * last modified: 09/06/2020
 * 
 * See class's Javadoc, listed just below. With the exception of the main class,
 * standard Java documentation is used so that Netbeans can use it when giving
 * me a summary of the class. 
 * 
 ******************************************************************************/

package Project1.Shapes;

import static org.lwjgl.opengl.GL11.*;

/**
 * This is the base class for shapes.
 * Shapes can draw themselves and track their own color.
 */
public abstract class Shape {

	// White was added for testing,
	// and pink as added because my lil' sister told me to.
	public enum Color { WHITE, RED, GREEN, BLUE, PINK };
	private Color drawColor;
	private boolean willDebugNextDraw;

	/**
	 * Constructs a shape with the given color.
	 * @param drawColor Color to use when drawing the shape.
	 */
	public Shape(Color drawColor) {
		this.drawColor = drawColor;

		this.willDebugNextDraw = false;
	}

	public void debugNextDraw() {
		willDebugNextDraw = true;
	}
	
	/**
	 * Draws the shape.
	 * 
	 * Should only be called between glBegin(GL_POINTS) and glEnd().
	 */
	public void draw() {
		applyColor();
		glPointSize(1);

		specificDraw(willDebugNextDraw);
		willDebugNextDraw = false;
	}
	
	/**
	 * Inheritors extend this in order to do shape-specific drawing code.
	 * @param debugDraw 
	 */
	protected abstract void specificDraw(boolean debugDraw);

	/**
	 * Tells our GFX library to specificDraw the shape using its color.
	 */
	private void applyColor() {
		// TODO
		switch(drawColor) {
			case WHITE:
				glColor3f(1.0f, 1.0f, 1.0f);
				break;
			case RED:
				glColor3f(1.0f, 0.0f, 0.0f);
				break;
			case GREEN:
				glColor3f(0.0f, 1.0f, 0.0f);
				break;
			case BLUE:
				glColor3f(0.0f, 0.0f, 1.0f);
				break;
			case PINK:
				glColor3f(1.0f, 0.75f, 0.79f);
				break;
			default:
				throw new AssertionError(drawColor.name());
		}
	}

	/**
	 * Changes the color which will be used for drawing the shape.
	 * @param drawColor New color to specificDraw the shape with.
	 */
	public void setColor(Color drawColor) {
		this.drawColor = drawColor;
	}

	/**
	 * Gets the current specificDraw color of the shape.
	 * @return The current specificDraw color.
	 */
	public Color getColor() {
		return drawColor;
	}

	/**
	 * Switches to the "next" color.
	 */
	public void rotateColor() {
		switch(drawColor) {
			case WHITE:
				drawColor = Color.RED;
				break;
			case RED:
				drawColor = Color.GREEN;
				break;
			case GREEN:
				drawColor = Color.BLUE;
				break;
			case BLUE:
				drawColor = Color.PINK;
				break;
			case PINK:
				drawColor = Color.WHITE;
				break;
			default:
				throw new AssertionError(drawColor.name());
			
		}	
	}

}