/******************************************************************************
 *       file: ShapeBuilder.java
 *     author: Daniel Edwards
 *      class: CS 4450
 * assignment: Program 1
 * 
 * last modified: 09/02/2020
 * 
 * See class's Javadoc, listed just below. With the exception of the main class,
 * standard Java documentation is used so that Netbeans can use it when giving
 * me a summary of the class. 
 * 
 ******************************************************************************/

package Project1.Shapes;

import org.lwjgl.util.Point;

/**
 * Builds shapes based on a string.
 * 
 * See the Build method for how the building functions.
 */
public final class ShapeBuilder {

	/**
	 * Controls whether we print shapes' debugging info as we build them.
	 */
	private boolean debugBuilding;

	/**
	 * Controls whether we shapes print debugging info as they first draw.
	 */
	private boolean debugDrawing;
	
	/**
	 * Creates a new shape builder object.
	 */
	public ShapeBuilder() {
		debugBuilding = false;
		debugDrawing = false;
	}
	
	/**
	 * Changes the debug flags.
	 * @param debugBuilding If true, building shapes will yield debug info.
	 * @param debugDrawing If true, built shapes will yield debug info when they draw.
	 */
	public void setDebugFlags(boolean debugBuilding, boolean debugDrawing)
	{
		this.debugBuilding = debugBuilding;
		this.debugDrawing = debugDrawing;
	}
	
	/**
	 * Builds a shape based on the input string from the file.
	 * 
	 * This is set up to just accept a string that came right out of the input
	 * file. Shapes are built based on each of the following strings. Note that
	 * all numbers can only be integers.
	 * 
	 *  c x,y r
	 * Generates a circle with center (x, y) and radius r.
	 * 
	 *  e x,y rx,ry
	 * Generates an ellipse with center (x, y), x radius rx, and y radius ry.
	 * 
	 *  l x1,y1 x2,y2
	 * Generates a line with endpoints (x1, y1) and (x2, y2).
	 * 
	 * @param line Line from the file.
	 * @return Returns a shape based on the given line.
	 */
	public Shape build(String line) {
		Shape result = null;

		if(!line.isBlank()) {
			String[] words = line.split(" ");

			try {
				result = parseWords(words);

				if(debugBuilding) {
					System.out.println(line + " -> " + result);
				}

				if(debugDrawing) {
					result.debugNextDraw();
				}
			}
			catch(RuntimeException e) {
				throw new IllegalArgumentException("Malformed line: " + line, e);
			}
		}

		return result;
	}

	/**
	 * Takes an array of words. This assumes a non-empty list of words.
	 * 
	 * Accepts the following strings:
	 * 
	 *  c x,y r
	 * Generates a circle with center (x, y) and radius r.
	 * 
	 *  e x,y rx,ry
	 * Generates an ellipse with center (x, y), x radius rx, and y radius ry.
	 * 
	 *  l x1,y1 x2,y2
	 * Generates a line with endpoints (x1, y1) and (x2, y2).
	 * 
	 * @param words
	 * @return 
	 */
	private Shape parseWords(String[] words) {
		Shape result = null;

		if(words[0].length() == 1) {
			switch(words[0].charAt(0)) {
				case 'c':
					result = new Circle(
						parsePoint(words[1]),
						Integer.parseInt(words[2]),
						Shape.Color.BLUE
					);
					break;

				case 'e':
					result = new Ellipse(
						parsePoint(words[1]),
						parsePoint(words[2]),
						Shape.Color.GREEN
					);
					break;

				case 'l':
					result = new Line(
						parsePoint(words[1]),
						parsePoint(words[2]),
						Shape.Color.RED
					);
					
					break;

				default:
					throw new IllegalArgumentException("Illegal first word: " + words[0]);
			}
		}
		else {
			throw new IllegalArgumentException("Illegal first word: " + words[0]);
		}

		return result;
	}

	/**
	 * Takes a string formatted as "x,y" and returns an appropriate point.
	 * @param word Word in format of "x,y".
	 * @return The point.
	 */
	private Point parsePoint(String word) {
		String[] coords = word.split(",");

		if(coords.length != 2) {
			throw new IllegalArgumentException("Malformed point: " + word); 
		}
		else {
			return new Point(
				Integer.parseInt(coords[0]),
				Integer.parseInt(coords[1])
			);
		}
	}
}
