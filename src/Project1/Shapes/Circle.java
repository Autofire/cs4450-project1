/******************************************************************************
 *       file: Circle.java
 *     author: Daniel Edwards
 *      class: CS 4450
 * assignment: Program 1
 * 
 * last modified: 09/06/2020
 * 
 * See class's Javadoc, listed just below. With the exception of the main class,
 * standard Java documentation is used so that Netbeans can use it when giving
 * me a summary of the class. 
 * 
 ******************************************************************************/

package Project1.Shapes;

import static org.lwjgl.opengl.GL11.glVertex2i;
import org.lwjgl.util.*;

/**
 * This represents a circle which may be drawn on the screen.
 * 
 * The circle is stored as a center point and a radius.
 */
public class Circle extends Shape {

	private Point center;
	private int radius;

	/**
	 * Constructs a new circle object.
	 * 
	 * @param center Center point of the circle.
	 * @param radius The circle's radius.
	 * @param drawColor Initial color used for drawing the circle.
	 */
	public Circle(Point center, int radius, Color drawColor) {
		super(drawColor);
		this.center = center;
		this.radius = radius;
	}

	@Override
	protected void specificDraw(boolean debugDraw) {
		final int STEP_COUNT = 2000;
		final double MAX_ANGLE = 2*Math.PI;

		double stepSize = MAX_ANGLE / STEP_COUNT;

		if(debugDraw) {
			System.out.println("Drawing circle at " + center);
			System.out.println("   with raidus of " + radius);
			System.out.println(" and step size of " + stepSize);
		}

		for(double angle = 0; angle <= MAX_ANGLE; angle += stepSize) {
			double x = Math.cos(angle) * radius;
			double y = Math.sin(angle) * radius;

			glVertex2i(center.getX()+(int)x, center.getY()+(int)y);
		}


	}

	@Override
	public String toString() {
		return "Circle{" + "center=" + center + ", radius=" + radius + '}';
	}
	
}
