/******************************************************************************
 *       file: Ellipse.java
 *     author: Daniel Edwards
 *      class: CS 4450
 * assignment: Program 1
 * 
 * last modified: 09/06/2020
 * 
 * See class's Javadoc, listed just below. With the exception of the main class,
 * standard Java documentation is used so that Netbeans can use it when giving
 * me a summary of the class. 
 * 
 ******************************************************************************/

package Project1.Shapes;

import static org.lwjgl.opengl.GL11.glVertex2i;
import org.lwjgl.util.*;

/**
 * This represents an Ellipse which may be drawn to the screen.
 *
 * The ellipse is stored as a center point and two radii.
 */
public class Ellipse extends Shape {

	private Point center;
	private Point radii;

	/**
	 * Construct a new ellipse object.
	 * 
	 * @param center Center point of the ellipse.
	 * @param radii The X and Y radii of the ellipse.
	 * @param drawColor Initial color used for drawing the ellipse.
	 */
	public Ellipse(Point center, Point radii, Color drawColor) {
		super(drawColor);
		this.center = center;
		this.radii = radii;
	}
	
	@Override
	protected void specificDraw(boolean debugDraw) {
		final int STEP_COUNT = 2000;
		final double MAX_ANGLE = 2*Math.PI;

		double stepSize = MAX_ANGLE / STEP_COUNT;

		if(debugDraw) {
			System.out.println("Drawing eclipse at " + center);
			System.out.println("     with radii of " + radii);
			System.out.println("  and step size of " + stepSize);
		}

		for(double angle = 0; angle <= MAX_ANGLE; angle += stepSize) {
			double x = Math.cos(angle) * radii.getX();
			double y = Math.sin(angle) * radii.getY();

			glVertex2i(center.getX()+(int)x, center.getY()+(int)y);
		}
	}

	@Override
	public String toString() {
		return "Ellipse{" + "center=" + center + ", radii=" + radii + '}';
	}
	
}
